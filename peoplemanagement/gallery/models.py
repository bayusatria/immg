from django.db import models
from django.utils.text import slugify
from PIL import Image
import os
from peoplemanagement.settings import THUMB_SIZE
from io import BytesIO
from django.core.files.base import ContentFile

class Type(models.Model):
    type_desc = models.CharField(max_length=50)
    def __str__(self):
        return u'{0}'.format(self.type_desc)

class Category(models.Model):
    type = models.ForeignKey(Type, on_delete=models.SET_NULL, null= True)
    category_desc = models.CharField(max_length=50)
    slug = models.SlugField(blank=True, editable=False)

    def __str__(self):
        return u'{0}'.format(self.category_desc)
    
    def save(self):
        self.slug = slugify(self.category_desc)
        super().save()

class Photo(models.Model):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    image_desc = models.CharField(max_length=50)
    image = models.ImageField(upload_to='galery_pics')
    image_thumbs = models.ImageField(upload_to='galery_thumbs', null=True, blank=True, editable=False)
    slug = models.SlugField(blank=True, editable=False)

    def __str__(self):
        return u'{0}'.format(self.image_desc)

    def save(self):
        self.slug = slugify(self.category.category_desc + ' ' + self.image_desc)
        # self.image_thumbs = self.image
        # super().save()

        if not self.make_thumbnail():
            # set to a default thumbnail
            raise Exception('Could not create thumbnail - is the file type valid?')

        super(Photo, self).save()   

    def make_thumbnail(self):

        image = Image.open(self.image)
        image.thumbnail(THUMB_SIZE, Image.ANTIALIAS)

        thumb_name, thumb_extension = os.path.splitext(self.image.name)
        thumb_extension = thumb_extension.lower()

        thumb_filename = thumb_name + '_thumb' + thumb_extension

        if thumb_extension in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        else:
            return False    # Unrecognized file type

        # Save thumbnail to in-memory file as StringIO
        temp_thumb = BytesIO()
        image.save(temp_thumb, FTYPE)
        temp_thumb.seek(0)

        # set save=False, otherwise it will run in an infinite loop

        self.image_thumbs = thumb_filename.replace("galery_pics","galery_thumbs")
        self.image_thumbs.save(thumb_filename.replace("galery_pics/",""), ContentFile(temp_thumb.read()), save=False)
        temp_thumb.close()

        return True



